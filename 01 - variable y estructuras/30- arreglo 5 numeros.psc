Proceso arreglo
	definir may,men,prom,num Como Real;
	definir i como entero;
	
	dimension num[6];
	
	para i<-1 hasta 5 con paso 1 Hacer
		escribir "ingrese el numero de la posicion: ",i;
		leer num[i];
	FinPara
	
	prom<-0;
	may<-0;
	men<-num[1];
	
	para i<-1 hasta 5 con paso 1 Hacer
		
		prom<-num[i] + prom;
		
		si num[i]>may Entonces
			may<-num[i];
		FinSi
		
		si num[i]<men Entonces
			men<-num[i];
		FinSi
	FinPara
	
	prom<-prom/5;
	
	escribir "el promedio es: ",prom;
	escribir "el mayor es: ",may;
	escribir "el menor es: ",men;
	
FinProceso
