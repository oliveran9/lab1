Proceso arreglopalabras
	definir palabra Como Caracter;
	definir corta,larga Como Caracter;
	definir i,control como entero;
	
	dimension palabra[999];
	
	control<-0;
	corta<-"lalaland";
	larga<-"lalaland";
	
	para i<-0 hasta 4 con paso 1 Hacer
		
		escribir "ingrese una palabra";
		leer palabra[i];
		
		si control=0 entonces
			
			corta<-palabra[0];
			larga<-palabra[0];
			control<-1;
			
		FinSi
		
		si Longitud(palabra[i])<longitud(corta) Entonces
			
			corta<-palabra[i];
			
		FinSi
		
		si longitud(palabra[i])>longitud(larga) Entonces
			larga<-palabra[i];
			
		FinSi
	FinPara
	
	para i<-0 hasta 4 con paso 1 Hacer
		escribir "palabra del lugar ",i,": ",palabra[i];
	FinPara
	
	escribir "palabra mas corta: ",corta;
	escribir "palabra mas larga: ",larga;
	
FinProceso
