Proceso numaleusuario
	definir num,i,j,control,resp,lugar,acum,bandera Como Entero;
	
	dimension num[999];
	
	control<-0;
	resp<-0;
	acum<-0;
	bandera<-0;
	
	para i<-0 hasta 99 con paso 1 Hacer
		
		num[i]<- ALEATORIO(0,20);
		
	FinPara
	
	escribir "tenemos 100 numeros aleatorios del 0 al 20 cargados, desea cambiar alguno ?: si=1 no=0";
	leer resp;
	
	mientras resp=1 hacer
		
		escribir "indique el lugar que desea reemplazar en numeros del 1 al 100";
		leer control;
		
		control<-control-1;
		
		escribir "el numero en este lugar es: ",num[control]," desea cambiarlo ? si=1 no=0";
		leer resp;
		
		si resp=1 Entonces
			escribir "ingrese el numero: ";
			leer num[control];
		FinSi
		
		resp<-0;
		
		escribir "se cambio el numero con exito, desea cambiar otro? si=1 no=0";
		leer resp;
		
		bandera<-1;
		
	FinMientras
	
    para j<-0 hasta 99 con paso 1 Hacer
		
		para i<-0 hasta 99 con paso 1 hacer
			
			si j=num[i] entonces 
				
				acum<-acum+1;
			FinSi
		FinPara
		
		si acum<>0 Entonces
			
			escribir "el numero ",j," se repite: ",acum;
			
		FinSi
		
		acum<-0;
		
	FinPara
	
	si bandera=1 Entonces
		
		escribir "uno o mas numeros fueron modificados por el usuario";
		
	FinSi
FinProceso
