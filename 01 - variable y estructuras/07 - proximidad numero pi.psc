Proceso proximidadpi
	
	definir n,operando,num,control,i como entero;
	definir resultado,acumulador como real;

	
	escribir "ingrese la cantidad de terminos de la suma que desea ver" ;
	leer n;
	
	acumulador <- 1 ;
	control <- 0;
	num <- 3;
	
	Para i<-0 Hasta n Con Paso 1 Hacer
		
		control <- i + 1;
		
		operando <- control mod 2;
		
		
		si operando = 0 Entonces
		
			
			acumulador <- acumulador + 1/num ;
			
			num <- num +2;
			
		siNo 
			
			acumulador <- acumulador - 1/num ;
			
			num <- num + 2;
			
		FinSi
		
		
	FinPara
	
	
	resultado <- acumulador * 4;
	
	escribir "en terminos : ", n;
	escribir "el resultado fue : ", resultado;
	
FinProceso
